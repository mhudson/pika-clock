{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Map.Strict
import Data.Time.Clock
import Data.Time.Calendar
import Data.Time.LocalTime
import Web.Scotty

images :: Map String FilePath
images = fromList [
    ("sleepy", "./images/sleepy.gif"),
    ("waking", "./images/waking.gif"),
    ("coffee", "./images/coffee.gif"),
    ("working", "./images/work.gif"),
    ("lunch", "./images/lunch.gif"),
    ("tired", "./images/tired.gif"),
    ("chillin", "./image/chillin.gif"),
    ("confused", "./images/acid.gif")
  ]

tenPm         = TimeOfDay { todHour = 22, todMin = 00, todSec = 0}
eightAm       = TimeOfDay { todHour = 08, todMin = 00, todSec = 0}
tenAm         = TimeOfDay { todHour = 10, todMin = 00, todSec = 0}
tenThirtyAm   = TimeOfDay { todHour = 10, todMin = 30, todSec = 0}
onePm         = TimeOfDay { todHour = 13, todMin = 00, todSec = 0}
twoPm         = TimeOfDay { todHour = 14, todMin = 00, todSec = 0}
twoThirtyPm   = TimeOfDay { todHour = 14, todMin = 30, todSec = 0}
blazeIt       = TimeOfDay { todHour = 16, todMin = 20, todSec = 0}
shelfIt       = TimeOfDay { todHour = 16, todMin = 21, todSec = 0}
fourThirtyPm  = TimeOfDay { todHour = 16, todMin = 30, todSec = 0}

getImage :: IO FilePath
getImage = do
  currentTime <- getCurrentTime
  timezone <- getCurrentTimeZone
  let now = localTimeOfDay $ utcToLocalTime timezone currentTime
  pure $ findWithDefault "confused" (currentMood now) images
  where
    currentMood now | now <= eightAm                              = "sleepy"
                    | now > eightAm       && now <= tenAm         = "waking"
                    | now > tenAm         && now <= tenThirtyAm   = "coffee"
                    | now > tenThirtyAm   && now <= midday        = "working"
                    | now > midday        && now <= onePm         = "lunch"
                    | now > onePm         && now <= twoPm         = "working"
                    | now > twoPm         && now <= twoThirtyPm   = "coffee"
                    | now > twoThirtyPm   && now <= blazeIt       = "working"
                    | now > blazeIt       && now <= shelfIt       = "confused"
                    | now > shelfIt       && now <= fourThirtyPm  = "working"
                    | now > fourThirtyPm  && now <= tenPm         = "tired"
                    | now > tenPm                                 = "sleepy"
                    | otherwise                                   = "confused"

main :: IO ()
main = scotty 3000 $ get "/" $ do
    setHeader "Cache-Control" "no-store, must-revalidate"
    img <- liftAndCatchIO $ getImage
    file img


